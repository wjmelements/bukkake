#ifndef RUNTIME_H
#define RUNTIME_H

#include "jit.h"
#include "barrier.h"

// initialize a function for this line number
func bu_init(uint64_t line_number);
// free a function created by this runtime
void bu_free(func line);
// prepare to call a function at this line number
void bu_enter(uint64_t line_number);

/** **  Current Function  ** **/

// close current function
void bu_close(char* filename, unsigned long line_number);
// increment *ptr
void bu_plus(int should_lock);
// decrement *ptr
void bu_minus(int should_lock);
// add to *ptr
void bu_add(int8_t offset, int should_lock);
// increment ptr
void bu_right(void);
// decrement ptr
void bu_left(void);
// add offset to ptr
void bu_chg8(int8_t offset);
// add offset to ptr
void bu_chg32(int32_t offset);
// enter loop
void bu_loop(void);
// exit loop, error if appropriate
void bu_end(char* filename, unsigned long line_number);
// wait *ptr to be zero
void bu_waitzero();
// putchar *ptr
void bu_put(void);
// *ptr = getchar
void bu_get(void);
// relative clone
void bu_clone(void);
// barrier
void bu_block(struct barrier* block);

#endif
