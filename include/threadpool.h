#ifndef THREADPOOL_H
#define THREADPOOL_H

#include "jit.h"

struct threadpool;
// returns a new initialized threadpool
struct threadpool* threadpool_new();
// initializes a threadpool
void threadpool_init(struct threadpool* this);
// free a threadpool and all its contents; calls threadpool_destroy
void threadpool_delete(struct threadpool* this);
// prepare to free a threadpool returned by new whose threads have been joined
void threadpool_destroy(struct threadpool* this);
// create a new thread in this threadpool; threadsafe
void threadpool_spawn(struct threadpool* this, func exec, void* param);
// join with all threads in the threadpool; not threadsafe
void threadpool_joinall(struct threadpool* this);

#endif
