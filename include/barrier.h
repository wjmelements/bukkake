#ifndef BARRIER_H
#define BARRIER_H

struct barrier;
// return a new initialized barrier
struct barrier* barrier_new(void);
// initialize a barrier
void barrier_init(struct barrier*);
// destroy a barrier
void barrier_delete(struct barrier*);
// cleanup a barrier but don't free
void barrier_destroy(struct barrier*);
// register a new thread to arrive
void barrier_register(struct barrier*);
// deregisters a thread
void barrier_deregister(struct barrier*);
// arrive and wait for all all registered
void barrier_arrive(struct barrier*);

#endif
