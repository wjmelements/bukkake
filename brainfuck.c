#include "runtime.h"
#include "barrier.h"
#include "stack.h"
#include "threadpool.h"

#include <fcntl.h>
#include "stdio.h"
#include "errno.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/stat.h"
#include "sys/mman.h"

#define DATA_SIZE 100000
uint8_t* bu_data;
func lines[1];

// satisfy runtime.c
struct barrier** barriers;
unsigned long** line_barriers;
unsigned long* line_barrier_indices;
struct threadpool* pool;

int ignore(char c) {
    switch(c) {
        case '+':
        case '-':
        case '.':
        case ',':
        case '>':
        case '<':
        case ']':
        case '[':
            return 0;
        default:
            return 1;
    }
}

int main(int argc, char* argv[]) {
    bu_data = (uint8_t*) calloc(DATA_SIZE, sizeof(uint8_t*));
    int i;
    for (i = 1; i < argc; i++) {
        // load file
        char* filename = argv[i];
        int fd = open(filename, O_RDONLY);
        if (fd == -1) {
            perror(filename);
            exit(errno);
        }
        struct stat filestatus;
        if (fstat(fd, &filestatus) == -1) {
            perror(filename);
            exit(errno);
        }
        char* exec = NULL;
        if (filestatus.st_size) {
            exec = mmap(exec, filestatus.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
            if (exec == MAP_FAILED) {
                perror(filename);
                exit(errno);
            }

            // jit
            extern struct stack* bu_loops;
            lines[0] = bu_init(0);
            unsigned int line_number = 0;
            unsigned int pc;
            for (pc = 0; pc < filestatus.st_size; pc++) {
                int8_t offset;
                int32_t traversal;
                switch(exec[pc]) {
                    case '+': 
                        offset = 1;
                        while ((pc + 1 < filestatus.st_size && (exec[pc + 1] == '+')) || ignore(exec[pc + 1])) {
                            if (!ignore(exec[pc + 1])) {
                                offset++;
                            }
                            pc++;
                        }
                        if (offset == 1) {
                            bu_plus(0);
                        } else {
                            bu_add(offset, 0);
                        }
                        break;
                    case '-':
                        offset = -1;
                        while ((pc + 1 < filestatus.st_size && (exec[pc + 1] == '-')) || ignore(exec[pc + 1])) {
                            if (!ignore(exec[pc + 1])) {
                                offset--;
                            }
                            pc++;
                        }
                        if (offset == -1) {
                            bu_minus(0);
                        } else {
                            bu_add(offset, 0);
                        }
                        break;
                    case '>':
                    case '<':
                        traversal = 0;
                        offset = 0; // use as continue var
                        while (pc < filestatus.st_size) {
                            switch (exec[pc]) {
                                case '>':
                                    traversal++;
                                    offset = 1;
                                    break;
                                case '<':
                                    traversal--;
                                    offset = 1;
                                    break;
                                default:
                                    break;
                            }
                            if (offset || ignore(exec[pc])) {
                                pc++;
                                offset = 0;
                            } else {
                                break;
                            }
                        }
                        pc--;
                        if (traversal == 1) {
                            bu_right();
                        } else if (traversal == -1) {
                            bu_left();
                        } else if (traversal < 127 || traversal > -128) {
                            bu_chg8(traversal);
                        } else {
                            bu_chg32(traversal);
                        }
                        break;
                    case '[':
                        while (pc + 1 < filestatus.st_size && ignore(exec[pc + 1])) {
                            pc++;
                        }
                        if (pc + 1 < filestatus.st_size && exec[pc + 1] == ']') {
                            bu_waitzero();
                            pc++;
                        } else {
                            bu_loop();
                        }
                        break;
                    case ']':
                        bu_end(filename, line_number + 1);
                        break;
                    case '.':
                        bu_put();
                        break;
                    case ',':
                        bu_get();
                        break;
                    case '\n':
                        line_number += 1;
                        break;
                }
            }
            // FIXME hack to circumvent bu_close, which jits bu_exit
            jit(0xc3);

            /* DEBUG
            for(uint8_t* prog = (uint8_t*) enter; *prog != 0xc3; prog++) {
                printf("%x\t", *prog);
            }
            printf("c3\n");
            */

            if (munmap(exec, filestatus.st_size)) {
                perror(filename);
                exit(errno);
            }
            if (close(fd)) {
                perror(filename);
                exit(errno);
            }

            lines[0](bu_data + DATA_SIZE / 2);
            size_t j;
            // TODO free their members
        } else {
            if(close(fd)) {
                perror(filename);
                exit(errno);
            }
            // empty file
        }
    }
    free(bu_data);
    return EXIT_SUCCESS;
}
