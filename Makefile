SHELL=/bin/bash
CC=gcc
INCLUDE=$(addprefix -I,include/)
CFLAGS=-O3 -pthread -march=x86-64 $(INCLUDE)
DEPS=$(addprefix lib/,stack.o runtime.o jit.o barrier.o threadpool.o)
.SECONDARY: $(DEPS)
EXEC=$(addprefix bin/, bukkake)
BK=$(addprefix bin/, bukkake)
BF=$(addprefix bin/, brainfuck)
EXECS=$(addprefix bin/, bukkake brainfuck)
BKCASES=hello q parahello io
BFCASES=hanoi mandelbrot
TIMEOUTS=gtimeout timeout 
MKDIRS=lib bin .pass

.PHONY: all run again gdb clean check testbk testbf Makefile
all: $(EXECS)
Makefile:
$(MKDIRS):
	@mkdir $@
bin/%: %.c $(DEPS) | bin
	$(CC) $(CFLAGS) $^ -o $@
lib/%.o: src/%.c | lib
	$(CC) $(CFLAGS) -c $^ -o $@
run:
	$(EXEC) examples/hello.bk
gdb:
	gdb $(EXEC)
clean:
	@rm -rf $(MKDIRS)
	@rm -f *.diff # failures in make check
again: clean all
# discern timeout
find_timeout=\
	TIMEOUT=;\
	for option in $(TIMEOUTS); do \
		if (type $$option > /dev/null 2>/dev/null);\
			then \
			TIMEOUT=$$option;\
			break;\
		fi;\
	done;\
    which $$TIMEOUT &> /dev/null || echo "Required: install a timeout program.";\
    which $$TIMEOUT &> /dev/null || exit 1;
check: testbk testbf
.pass/%: examples/%.bk $(BK) | .pass
	@$(find_timeout)\
	printf "$*: "; \
	$$TIMEOUT 1s $(BK) $< | diff - ok/$*.ok > $*.diff\
		&& echo -e "\033[0;32mpass\033[0m" \
		|| echo -e "\033[0;31mfail\033[0m" && cat $*.diff; \
	find $*.diff -empty -type f -delete ;\
	touch $@
.pass/io: examples/io.bk $(BK) | .pass
	@$(find_timeout)\
	printf "$(@F): "; \
	$$TIMEOUT 1s $(BK) $< < ok/q.ok | diff - ok/$(@F).ok > $(@F).diff\
		&& echo -e "\033[0;32mpass\033[0m" \
		|| echo -e "\033[0;31mfail\033[0m" && cat $(@F).diff; \
	find $(@F).diff -empty -type f -delete ;\
	touch $@
.pass/%: examples/%.b $(BF) | .pass
	@$(find_timeout)\
	printf "$*: "; \
	$$TIMEOUT 3s $(BF) $< | diff - ok/$*.ok > $*.diff\
		&& echo -e "\033[0;32mpass\033[0m" && touch $@ \
		|| echo -e "\033[0;31mfail\033[0m" && cat $*.diff; \
	find $*.diff -empty -type f -delete ;
testbk: $(addprefix .pass/,$(BKCASES))
testbf: $(addprefix .pass/,$(BFCASES))
