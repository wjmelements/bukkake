#include "threadpool.h"
#include "pthread.h"
#include "stdlib.h"
struct threadnode {
    pthread_t thread;
    struct threadnode* next;
};
struct threadpool {
    struct threadnode* in;
    struct threadnode* out;
};
void threadpool_init(struct threadpool* this) {
    this->in = malloc(sizeof(struct threadnode));
    this->out = this->in;
    this->in->next = NULL;
}
struct threadpool* threadpool_new() {
    struct threadpool* ret = malloc(sizeof(struct threadpool));
    threadpool_init(ret);
    return ret;
}
void threadpool_spawn(struct threadpool* this, func exec, void* param) {
    struct threadnode* swap = malloc(sizeof(struct threadnode));
    struct threadnode* const new = swap;
    pthread_create(&new->thread, NULL, exec, param);
    new->next = NULL;
    asm volatile (
        "xchg %0, %1"
        : "=r" (swap),
          "=m" (this->in)
        : "0" (swap),
          "m" (this->in)
    );
    // swap is now old in
    swap->next = new;
}
void threadpool_joinall(struct threadpool* this) {
    while (this->out->next) {
        struct threadnode* pop = this->out;
        pthread_join(pop->next->thread, NULL);
        this->out = pop->next;
        free(pop);
    }
}
void threadpool_destroy(struct threadpool* this) {
    free(this->out);
}
void threadpool_delete(struct threadpool* this) {
    threadpool_destroy(this);
    free(this);
}
