#include "jit.h"
#include "stdlib.h"
#include "sys/mman.h"
uint8_t* jit_ptr;
const size_t jit_blocksize = 0x40000;
func jit_init(uint8_t** old) {
    if (old) {
        *old = jit_ptr;
    }
    jit_ptr = mmap(NULL, jit_blocksize, PROT_READ | PROT_WRITE | PROT_EXEC, 
    #ifdef __GLIBC__ 
    MAP_ANONYMOUS
    #else
    MAP_ANON
    #endif
    | MAP_PRIVATE, -1, 0);
    return (func) jit_ptr;
}
void jit_free(func block) {
    munmap(block, jit_blocksize);
}
func jit_set(uint8_t* start, uint8_t** old) {
    if (old) {
        *old = jit_ptr;
    }
    jit_ptr = start;
    return (func) jit_ptr;
}
uint8_t* jit_get(void) {
    return jit_ptr;
}
void jit(uint8_t one) {
    *jit_ptr = one;
    jit_ptr++;
}
void jit2(uint8_t one, uint8_t two) {
    jit(one);
    jit(two);
}
void jit3(uint8_t one, uint8_t two, uint8_t three) {
    jit(one);
    jit2(two, three);
}
void jit4(uint8_t one, uint8_t two, uint8_t three, uint8_t four) {
    jit2(one, two);
    jit2(three, four);
}
void jit_align(void) {
    while ((unsigned long)jit_ptr % 4) {
        jit(0x90);
    }
}
void jit32(uint32_t constant) {
    size_t i;
    for (i = 0; i < 4; i++) {
        jit(constant & 0xFF);
        constant >>= 8;
    }
}
void jit64(uint64_t constant) {
    size_t i;
    for (i = 0; i < 8; i++) {
        jit(constant & 0xFF);
        constant >>= 8;
    }
}
int64_t jit_relative(uint64_t loc) {
    return loc - (int64_t) jit_ptr;
}
