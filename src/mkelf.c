#include <arpa/inet.h>
#include <assert.h>
#include <elf.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/utsname.h>
int write_header(int fd) {
    Elf64_Ehdr header;
    header.e_ident[EI_MAG0] = ELFMAG0;
    header.e_ident[EI_MAG1] = ELFMAG1;
    header.e_ident[EI_MAG2] = ELFMAG2;
    header.e_ident[EI_MAG3] = ELFMAG3;
    header.e_ident[EI_CLASS] = ELFCLASS64;
    //header.e_ident[EI_DATA] = ELFDATANONE;
    if (htonl(47) == 47) {
        header.e_ident[EI_DATA] = ELFDATA2MSB;
    } else {
        header.e_ident[EI_DATA] = ELFDATA2LSB;
    }
    header.e_ident[EI_VERSION] = EV_CURRENT;
    struct utsname sys;
    uname(&sys);
    if (strncmp(sys.sysname,"Linux",5) == 0) {
        header.e_ident[EI_OSABI] = ELFOSABI_LINUX;
    } else {
        // TODO support more
        header.e_ident[EI_OSABI] = ELFOSABI_NONE;
    }
    header.e_ident[EI_ABIVERSION] = 0;
    memset(&header.e_ident[EI_PAD], 0, EI_NIDENT - EI_PAD);
    header.e_type = ET_EXEC;
    header.e_machine = EM_X86_64;
    header.e_version = EV_CURRENT;
    header.e_entry = 0; // VA of entry point
    header.e_phoff = 0; // segment table offset in bytes
    header.e_shoff = 0; // section header table's offset in bytes
    header.e_flags = 0;
    header.e_ehsize = sizeof(header);
    header.e_phentsize = sizeof(Elf64_Phdr); // size of entry in program header table
    header.e_phnum = 0; // number of entries in PHT
    header.e_shentsize = sizeof(Elf64_Shdr); // section header entry size
    header.e_shnum = 0; // number of entries in SHT
    header.e_shstrndx = SHN_UNDEF; // section name string table

    write(fd, &header, sizeof(header));
    return 0;
}
int write_segment_table(int fd) {
    return 0;
}
int write_text(int fd) {
    return 0;
}
int write_rodata(int fd) {
    return 0;
}
int write_data(int fd) {
    return 0;
}
int write_section_table(int fd) {
    return 0;
}
int write_exec(char* outfile) {
    int fd = open(outfile, O_CREAT, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH); // chmod 755
    if (fd == -1) {
        perror(outfile);
    }
    int ret = write_header(fd)
        || write_segment_table(fd)
        || write_text(fd)
        || write_rodata(fd)
        || write_data(fd)
        || write_section_table(fd)
    ;
    if (ret) {
        perror(outfile);
    }
    close(fd);
}
